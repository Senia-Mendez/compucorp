# Compucorp #

This is the Compucorp Test repo created for the purpose of demonstrating skill set related to git, html, css, drupal etc.

# Approach #

### Hosting ###

The site is hosted using Acquia free account

### SASS ###

* SMACSS folder structure with a twist and BEM naming convention
* SASS Autocompile to output SASS files

### Drupal ###

After creating Acquia site, I installed Acquia desktop and link to the hosted site.

* Add Views UI module
* Utilize content types
* Create views and override them in order to apply my own styles