<?php
/**
* preprocess hook
*/
function compucorp_preprocess_page(&$vars, $hook) {
    unset($vars['page']['content']['system_main']['default_message']);
}

/**
* Hook for links__system_main_men
* main menu will display this instead of the default
*/
function compucorp_links__system_main_menu($variables) {
  $html = "<div>\n";
  $html .= "<a class='mobile-close' href='#'>×</a>";
  $html .= "  <ul class='menu links right'>\n";
  $title = "Join Us";

  foreach ($variables['links'] as $link) {

    if($link['title'] === $title){
      $link['attributes']['class'][] = 'btn btn--primary btn--join';
    }

    $html .= "<li class='menu__links'>".l($link['title'], $link['href'], $link)."</li>";
  }

  $html .= "  </ul>\n";
  $html .= "</div>\n";

  return $html;
}
?>
