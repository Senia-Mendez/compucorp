(function ($) {

  $(document).ready(function(){

    // display the main menu when clicked
    $('.mobile-toggle').click(function(){
      $('.main-menu').addClass('main-menu--open');
    });

    //close main menu
    $('.mobile-close').click(function(){
      $('.main-menu').removeClass('main-menu--open');
    });

  });

}(jQuery));
