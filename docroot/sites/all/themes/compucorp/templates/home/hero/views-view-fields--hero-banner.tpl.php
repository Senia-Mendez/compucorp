<div class="hero-banner"
    style="background-image: url('<?php print $fields["field_banner_image"]->content ?>');filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php print $fields["field_banner_image"]->content ?>', sizingMethod='scale'); -ms-filter: 'progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php print $fields["field_banner_image"]->content ?>', sizingMethod='scale')'';">

    <div class="container">
        <div class="table">
            <div class="banner-info table__table-cell table__table-cell--middle">
                <h1 class="banner-info__title">
                    <?php print filter_xss($fields["title"]->content) ?>
                </h1>

                <div class="info">
                    <?php print $fields["field_banner_info"]->content ?>
                </div>
            </div>
        </div>
    </div>
</div>
