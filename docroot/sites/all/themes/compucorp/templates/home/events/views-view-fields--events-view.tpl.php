<div class="container">
    <div class="table event">
        <div class="col-half table__table-cell table__table-cell--middle">
            <article class="m-b-5">
                <span class="text-color-primary text-semi-bold">Next Event:</span>
                <span class="event__date">
                    <span class="p-r-5">
                        <?php print filter_xss($fields["field_event_date"]->content) ?>
                    </span>
                    <?php print filter_xss($fields["field_event_time"]->content) ?>
                </span>
            </article>

            <article>
                <span class="event__detail">
                    <?php print filter_xss($fields["title"]->content) ?>,
                    <?php print filter_xss($fields["field_event_location"]->content) ?>
                    <?php print filter_xss($fields["field_event_address_1"]->content) ?>
                    <?php print filter_xss($fields["field_event_address_2"]->content) ?>
                </span>
            </article>
        </div>

        <div class="col-half table__table-cell table__table-cell--middle">
            <a class="btn btn--secondary event__register" href="#">Register</a>
        </div>
    </div>
</div>
