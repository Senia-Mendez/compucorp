<div class="col-full-width">

  <div class="container container--padded p-b-0">
    <!-- title -->
    <h2 class="text-center text-upper">Our Mission</h2>
    <!-- title END-->

    <p class="text-center font-sm text-color-secondary-gray">
      Charities and Partners collaborating and sharing open solutions and ideas to create value in the digital space.
    </p>

    <p class="text-center text-semi-bold text-color-secondary-gray">
      If you are a charity or a supplier, we are ready to welcome you.
    </p>

    <!-- content -->
    <?php if ($rows): ?>
      <?php print $rows; ?>
    <?php elseif ($empty): ?>

      <div class="view-empty">
        <?php print $empty; ?>
      </div>

    <?php endif; ?>
    <!-- content END-->
  </div>

</div>
