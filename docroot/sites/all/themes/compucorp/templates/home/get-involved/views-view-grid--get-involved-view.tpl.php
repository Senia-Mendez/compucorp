<?php foreach ($rows as $row_number => $columns): ?>
    <div class="row text-center m-b-20">

      <?php foreach ($columns as $column_number => $item): ?>
        <div class="col-third">
          <?php print $item; ?>
        </div>
      <?php endforeach; ?>

    </div>

    <div class="clear"></div>
<?php endforeach; ?>
