<div class="col-full-width background-white">

  <div class="container container--padded">
    <!-- title -->
    <h2 class="text-center text-upper">Get Involved</h2>
    <!-- title END-->

    <!-- content -->
    <?php if ($rows): ?>
      <?php print $rows; ?>
    <?php elseif ($empty): ?>

      <div class="view-empty">
        <?php print $empty; ?>
      </div>

    <?php endif; ?>
    <!-- content END-->
  </div>

</div>
