<div class="col-full-width">

  <div class="blog-container container--padded background-white ">
    <!-- title -->
    <h3 class="text-center text-upper">Blog</h3>
    <!-- title END-->

    <!-- content -->
    <?php if ($rows): ?>

      <div class="blog-btn-next">
        <i class="fa fa-angle-left" aria-hidden="true"></i>
      </div>

      <?php print $rows; ?>

      <div class="blog-btn-prev">
        <i class="fa fa-angle-right" aria-hidden="true"></i>
      </div>

    <?php elseif ($empty): ?>

      <div class="view-empty">
        <?php print $empty; ?>
      </div>

    <?php endif; ?>
    <!-- content END-->
  </div>

  <div class="blog-underlay"></div>

</div>
