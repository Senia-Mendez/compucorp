<?php foreach ($rows as $row_number => $columns): ?>

    <div class="row">

        <?php foreach ($columns as $column_number => $item): ?>
          <div class="col-fourth blog-post">
              <?php print $item; ?>
          </div>
        <?php endforeach; ?>

    </div>

<?php endforeach; ?>
