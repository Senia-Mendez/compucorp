<div class="col-full-width">

  <div class="container container--padded">

    <div class="p-r-15 p-l-15">
      <hr class="m-b-25">
    </div>

    <!-- title -->
    <h2 class="text-center text-upper">Our Members</h2>
    <!-- title END-->

    <!-- content -->
    <?php if ($rows): ?>
      <?php print $rows; ?>

      <div class="slidder-toggle">
        <span class="slidder-toggle__btn slidder-toggle__btn--active"></span>
        <span class="slidder-toggle__btn"></span>
        <span class="slidder-toggle__btn"></span>
      </div>
    <?php elseif ($empty): ?>

      <div class="view-empty">
        <?php print $empty; ?>
      </div>

    <?php endif; ?>
    <!-- content END-->
  </div>

</div>
