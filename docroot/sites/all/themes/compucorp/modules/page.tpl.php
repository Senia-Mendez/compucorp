<div class="page-wrapper">

  <!-- header -->
  <div class="header">
    <div class="container">

      <div class="table">

        <!-- logo -->
        <?php if ($logo): ?>
          <div class="table__table-cell table__table-cell--middle col-half">
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="logo-container">
              <img class="header__logo" src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
            </a>
          </div>
        <?php endif; ?>
        <!-- logo END-->

        <!-- navigation -->
        <?php if ($main_menu || $secondary_menu): ?>
          <div class="main-menu table__table-cell table__table-cell--middle col-half text-center">
            <?php print theme('links__system_main_menu', array('links' => $main_menu, 'attributes' => array('class' => array('menu', 'links', 'inline', 'right')))); ?>
          </div>

          <div class="mobile-toggle">
            <a href="#">
              <i class="fa fa-bars" aria-hidden="true"></i>
            </a>
          </div>
        <?php endif; ?>
        <!-- navigation END-->

      </div>

    </div>
  </div>
  <!-- header END-->

  <!-- banner -->
  <?php if ($page['banner']): ?>
    <?php print render($page['banner']); ?>
  <?php endif; ?>
  <!-- banner END-->

  <!-- Events -->
  <?php if ($page['event']): ?>
    <?php print render($page['event']); ?>
  <?php endif; ?>
  <!-- Events END-->

  <!-- content -->
  <div class="page">

    <?php print $messages; ?>

    <div class="main-wrapper">

      <div class="content column">

        <?php print render($page['content']); ?>

      </div>

    </div>

  </div>
  <!-- content END-->

  <!-- footer -->
  <div class="footer">
    <div class="social">
      <i class="fa fa-facebook" aria-hidden="true"></i>
      <i class="fa fa-twitter" aria-hidden="true"></i>
      <i class="fa fa-google-plus" aria-hidden="true"></i>
    </div>
    <?php print render($page['footer']); ?>
  </div>
  <!-- footer END-->

</div>
